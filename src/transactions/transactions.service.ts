import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { Repository } from 'typeorm';
import { Countrie } from 'src/countrie/entities/countrie.entity';
import { Transaction } from './entities/transaction.entity';
import { TYPES_TRANSACTIONS } from 'src/enums/types-transactions.enum';
import { TransactionsTrackin } from 'src/transactions-trackins/entities/transactions-trackin.entity';
import { STATUS_TRANSACTION } from 'src/enums/status-transactions.enum';
import { Fund } from 'src/funds/entities/fund.entity';
import { Movement } from 'src/movements/entities/movement.entity';
import { TYPES_MOVEMENTS } from 'src/enums/types-movements.enum';

@Injectable()
export class TransactionsService {
  constructor(
    @Inject('COUNTRIE_REPOSITORY')
    private countrieRepository: Repository<Countrie>,
    @Inject('FUND_REPOSITORY')
    private fundsRepository: Repository<Fund>,
    @Inject('TRANSACTION_REPOSITORY')
    private transactionRepository: Repository<Transaction>,
    @Inject('TRANSACTION_TRACKINS_REPOSITORY')
    private transactionTrackingRepository: Repository<TransactionsTrackin>,
    @Inject('MOVEMENTS_REPOSITORY')
    private movementRepository: Repository<Movement>,
  ) {}

  async create(
    {
      document,
      type_transaction,
      account_holder,
      bank,
      account_document,
      account,
      amount,
    }: CreateTransactionDto,
    user,
  ) {
    const findCountrie = await this.countrieRepository.findOne({
      where: { name: 'Venezuela' },
    });

    const fund = await this.fundsRepository
      .createQueryBuilder('fund')
      .where('fund.companyId = :companyId AND fund.countrieId = :countrieId', {
        companyId: user.company.id,
        countrieId: findCountrie.id,
      })
      .getOne();

    // Verificar si el monto solicitado excede el saldo disponible
    if (amount > fund.amount) {
      throw new HttpException(
        {
          status: HttpStatus.PAYMENT_REQUIRED, // Código 402 para indicar falta de saldo
          error:
            'No cuenta con disponibilidad para realizar esta transacción, contacte a administración y realice una recarga.',
        },
        HttpStatus.PAYMENT_REQUIRED,
      );
    }

    let montoResta =
      parseFloat(amount.toString()) * parseFloat(findCountrie.rate.toString());
    let amountNew =
      parseFloat(fund.amount.toString()) - parseFloat(montoResta.toString());

    await this.fundsRepository.update(fund.id, { amount: amountNew });

    let type = null;
    if (type_transaction === TYPES_TRANSACTIONS.PAGO) {
      type = TYPES_TRANSACTIONS.PAGO;
    } else {
      type = TYPES_TRANSACTIONS.TRANSFERENCIA;
    }

    // Validar si el cliente excede el monto mensual permitido
    const now = new Date();
    const firstDayOfMonth = new Date(now.getFullYear(), now.getMonth(), 1);
    const lastDayOfMonth = new Date(now.getFullYear(), now.getMonth() + 1, 0);

    const totalAmount = await this.transactionRepository
      .createQueryBuilder('trans')
      .select('SUM(trans.amount_received)', 'total')
      .where('trans.document = :document', { document })
      .andWhere('trans.created_at BETWEEN :start AND :end', {
        start: firstDayOfMonth,
        end: lastDayOfMonth,
      })
      .getRawOne();

    // Validar que no supere el límite de 1000 USD al mes
    if (parseInt(totalAmount.total) + parseInt(amount.toString()) >= 1000) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT, // Código 409 para indicar conflicto por exceder el monto
          error: `El cliente con documento ${document} ha excedido el límite legal permitido de 1000USD al mes.`,
        },
        HttpStatus.CONFLICT,
      );
    }

    const data = await this.transactionRepository.save({
      document,
      type_transaction: type,
      account_holder,
      bank,
      account_document,
      account,
      amount_received: amount,
      amount_destination: amount * findCountrie.rate,
      amount_rate: findCountrie.rate,
      company: user.company,
      creator: user,
    });

    await this.transactionTrackingRepository.save({
      status: STATUS_TRANSACTION.CREADA,
      transaction: data,
    });

    await this.movementRepository.save({
      type: TYPES_MOVEMENTS.TRANSACCION,
      old_amount: fund.amount,
      new_amount: amountNew,
      movement_amount: data.amount_destination,
      fund: fund,
      description: `Descuento de saldo por transaccion ${data.id}`,
    });

    const data2 = await this.transactionRepository.findOne({
      where: { id: data.id },
      relations: ['company', 'creator', 'bank'],
    });

    return {
      data: {
        transactions: data2,
        messageTicket:
          '<b>Terminos y Condiciones:</b> https://www.dolardaily.com/terminos-condiciones \n <b>Contacto:</b> Whatsapp: +58 4248647216 Correo: soporte@dolardaily.com',
      },
      message: 'Transacción creada con éxito',
    };
  }

  async findAll(user) {
    let transactions = await this.transactionRepository
      .createQueryBuilder('transaction')
      .leftJoinAndSelect('transaction.bank', 'bank')
      .leftJoinAndSelect('transaction.trackings', 'trackings');

    if (user.role === 'integrator') {
      transactions = transactions.where('transaction.creatorId = :creatorId', {
        creatorId: user.id,
      });
    }

    if (user.role === 'company') {
      transactions = transactions.where('transaction.companyId = :companyId', {
        companyId: user.company.id,
      });
    }

    transactions = transactions.orderBy('transaction.created_at', 'DESC');
    const result = await transactions.getMany();

    return { data: result, message: 'Transacciones obtenidas con exito' };
  }

  async findOne(id: string, user) {
    const transaction = await this.transactionRepository.findOne({
      where: { id },
    });

    if (!transaction) {
      throw new NotFoundException('No se ha encontrado esta transaccion');
    }

    return {
      data: {
        transaction,
        messageTicket:
          '<b>Terminos y Condiciones:</b> https://www.dolardaily.com/terminos-condiciones <br> <b>Contacto:</b> Whatsapp: +58 4248647216 <br> Correo: soporte@dolardaily.com',
      },
      message: 'Transaccion obtenida con exito',
    };
  }

  async cancel(id) {
    const transaction = await this.transactionRepository.findOne({
      where: { id },
    });
    console.log(transaction);
    if (!transaction) {
      throw new NotFoundException('No se ha encontrado esta transaccion');
    }

    const createdAt = transaction.created_at;
    const currentTime = new Date();

    const differenceInMinutes = Math.floor(
      (currentTime.getTime() - createdAt.getTime()) / (1000 * 60),
    );

    if (differenceInMinutes >= 5) {
      throw new BadRequestException(
        'Esta transaccion ya tiene mas de 5 minutos de creacion la misma se encuentra en proceso',
      );
    }

    await this.transactionRepository.update(id, {
      status: STATUS_TRANSACTION.ANULADA,
    });

    await this.transactionTrackingRepository.save({
      status: STATUS_TRANSACTION.ANULADA,
      transaction: transaction,
    });

    return { data: transaction, message: 'Transaccion anulada con exito' };
  }

  update(id: number, updateTransactionDto: UpdateTransactionDto) {
    return `This action updates a #${id} transaction`;
  }

  remove(id: number) {
    return `This action removes a #${id} transaction`;
  }
}
