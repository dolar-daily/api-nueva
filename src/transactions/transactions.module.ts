import { Module } from "@nestjs/common";
import { TransactionsService } from "./transactions.service";
import { TransactionsController } from "./transactions.controller";
import { DatabaseModule } from "src/database/database.module";
import { transactionProvider } from "./transactions.provider";
import { countrieProvider } from "src/countrie/countrie.provider";
import { transactionTrackingsProvider } from "src/transactions-trackins/transactions-trackins.provider";
import { fundProvider } from "src/funds/funds.provider";
import { bankProvider } from "src/bank/bank.provider";
import { movementProvider } from "src/movements/movements.provider";

@Module({
  imports: [DatabaseModule],
  controllers: [TransactionsController],
  providers: [
    ...transactionProvider,
    ...countrieProvider,
    ...transactionTrackingsProvider,
    ...fundProvider,
    ...bankProvider,
    ...movementProvider,
    TransactionsService,
  ],
})
export class TransactionsModule {}
