import {
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateIf,
} from "class-validator";
import { Bank } from "src/bank/entities/bank.entity";
import { TYPES_TRANSACTIONS } from "src/enums/types-transactions.enum";

export class CreateTransactionDto {
  @IsString()
  type_transaction: TYPES_TRANSACTIONS;

  @IsNotEmpty()
  document: string;

  @ValidateIf((o) => o.type_transaction === 'TRANSFERENCIA')
  @IsNotEmpty()
  account_holder: string;

  @ValidateIf((o) => o.type_transaction === 'PAGO')
  @IsNotEmpty()
  bank: Bank;

  @IsNotEmpty()
  account_document: string;

  @IsNotEmpty()
  account: string; //aqui se guarda ya sea el telefono o el numero de cuenta

  @IsNotEmpty()
  @IsNumber()
  amount: number;
}
