import { Bank } from "src/bank/entities/bank.entity";
import { Company } from "src/company/entities/company.entity";
import { STATUS_TRANSACTION } from "src/enums/status-transactions.enum";
import { TYPES_TRANSACTIONS } from "src/enums/types-transactions.enum";
import { TransactionsTrackin } from "src/transactions-trackins/entities/transactions-trackin.entity";
import { User } from "src/user/entities/user.entity";
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class Transaction {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  document: string;

  @Column({ nullable: true })
  account_holder: string;

  @ManyToOne(() => Bank, (bank) => bank.transactions, { nullable: true })
  bank: Bank;

  @Column()
  account_document: string;

  @Column()
  account: string;

  @Column({ type: "decimal", precision: 10, scale: 2, default: 0.0 })
  amount_received: number;

  @Column({ type: "decimal", precision: 10, scale: 2, default: 0.0 })
  amount_destination: number;

  @Column({ type: "decimal", precision: 10, scale: 2, default: 0.0 })
  amount_rate: number;

  @Column({
    type: "enum",
    enum: STATUS_TRANSACTION,
    default: STATUS_TRANSACTION.CREADA,
  })
  status: STATUS_TRANSACTION;

  @Column({
    type: 'enum',
    enum: TYPES_TRANSACTIONS
  })
  type_transaction: TYPES_TRANSACTIONS

  @ManyToOne(() => Company, (company) => company.transactions)
  company: Company

  @OneToMany(() => TransactionsTrackin, (tracking) => tracking.transaction, {eager: true})
  trackings: TransactionsTrackin[]

  @ManyToOne(() => User, (user) => user.transactions)
  creator: User

  @Column({nullable: true})
  comprobante: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;
}
