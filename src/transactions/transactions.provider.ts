import { Connection } from "typeorm";
import { Logger } from "@nestjs/common";
import { Transaction } from "./entities/transaction.entity";

const logger = new Logger("TRANSACTION PROVIDER");

export const transactionProvider = [
  {
    provide: "TRANSACTION_REPOSITORY",
    useFactory: (conecction: Connection) => {
      if (!conecction) {
        logger.error("La conexión a la base de datos no está definida");
        return null;
      }

      try {
        logger.log("Se ha cargado correctamente el Modelo: Transaction")
        return conecction.getRepository(Transaction);
      } catch (error) {
        logger.error("Error al cargar el Modelo: Transaction", error);
      }
    },
    inject: ["MYSQL_CONNECT"],
  },
];
