import { Controller, Get, Post, Body, Patch, Param, Delete, Request, UseGuards, UseInterceptors } from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { AuthGuard } from 'src/auth/guard/auth.guard';
import { TransformInterceptor } from 'src/interceptors/response.interceptor';
import { UuidValidatorPipe } from 'src/pipes/validations/client-id-validation/uuid-validator.pipe';

@Controller('transactions')
@UseInterceptors(TransformInterceptor)
@UseGuards(AuthGuard)
export class TransactionsController {
  constructor(private readonly transactionsService: TransactionsService) {}

  @Post()
  create(@Body() createTransactionDto: CreateTransactionDto, @Request() req) {
    return this.transactionsService.create(createTransactionDto, req.user);
  }

  @Get()
  findAll(@Request() req) {
    return this.transactionsService.findAll(req.user);
  }

  @Get(':id')
  findOne(@Param('id', new UuidValidatorPipe()) id: string, @Request() req) {
    return this.transactionsService.findOne(id, req.user);
  }

  @Get(':id/cancel')
  cancel(@Param('id', new UuidValidatorPipe()) id: string, @Request() req) {
    return this.transactionsService.cancel(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTransactionDto: UpdateTransactionDto) {
    return this.transactionsService.update(+id, updateTransactionDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.transactionsService.remove(+id);
  }
}
