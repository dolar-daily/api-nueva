import { IsOptional, IsString } from 'class-validator';

export class CreateCountrieDto {
  @IsString()
  name: string;

  @IsString()
  currency: string;

  @IsOptional()
  rate: number;
}
