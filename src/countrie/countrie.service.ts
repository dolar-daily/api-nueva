import { ConflictException, Inject, Injectable } from '@nestjs/common';
import { CreateCountrieDto } from './dto/create-countrie.dto';
import { UpdateCountrieDto } from './dto/update-countrie.dto';
import { Repository } from 'typeorm';
import { Countrie } from './entities/countrie.entity';

@Injectable()
export class CountrieService {
  constructor(
    @Inject('COUNTRIE_REPOSITORY')
    private countrieRepository: Repository<Countrie>,
  ) {}
  async create(createCountrieDto: CreateCountrieDto) {
    const findCountrie = await this.countrieRepository.findOne({
      where: { name: createCountrieDto.name },
    });
    if (findCountrie) {
      throw new ConflictException('Este pais ya existe');
    }

    const data = await this.countrieRepository.save(createCountrieDto);

    return { data, message: 'Pais Creado con exito' };
  }

  findAll() {
    return `This action returns all countrie`;
  }

  async findOne(id) {
    let data = await this.countrieRepository.findOne({ where: { id } });

    return { data, message: 'Pais obtenido con exito' };
  }

  async update(id, updateCountrieDto: UpdateCountrieDto) {
    let data = await this.countrieRepository.update(id, updateCountrieDto);

    return {data, message: 'Actualizado correctamente'}
  }

  remove(id: number) {
    return `This action removes a #${id} countrie`;
  }
}
