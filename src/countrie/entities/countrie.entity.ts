import { Fund } from "src/funds/entities/fund.entity";
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class Countrie {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  name: string;

  @Column()
  currency: string;

  @OneToMany(() => Fund, (fund) => fund.countrie)
  funds: Fund[];

  @Column({type: 'decimal', precision: 15, scale: 4, nullable: true})
  rate: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;
}
