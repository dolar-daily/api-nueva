import { Connection } from "typeorm";
import { Logger } from "@nestjs/common";
import { Countrie } from "./entities/countrie.entity";

const logger = new Logger("CONTRIE PROVIDER");

export const countrieProvider = [
  {
    provide: "COUNTRIE_REPOSITORY",
    useFactory: (conecction: Connection) => {
      if (!conecction) {
        logger.error("La conexión a la base de datos no está definida");
        return null;
      }

      try {
        logger.log("Se ha cargado correctamente el Modelo: Countrie")
        return conecction.getRepository(Countrie);
      } catch (error) {
        logger.error("Error al cargar el Modelo: Countrie", error);
      }
    },
    inject: ["MYSQL_CONNECT"],
  },
];
