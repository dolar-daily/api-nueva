import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UseGuards } from '@nestjs/common';
import { CountrieService } from './countrie.service';
import { CreateCountrieDto } from './dto/create-countrie.dto';
import { UpdateCountrieDto } from './dto/update-countrie.dto';
import { TransformInterceptor } from 'src/interceptors/response.interceptor';
import { AuthGuard } from 'src/auth/guard/auth.guard';

@Controller('countries')
@UseGuards(AuthGuard)
@UseInterceptors(TransformInterceptor)
export class CountrieController {
  constructor(private readonly countrieService: CountrieService) {}

  @Post()
  create(@Body() createCountrieDto: CreateCountrieDto) {
    return this.countrieService.create(createCountrieDto);
  }

  @Get()
  findAll() {
    return this.countrieService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id) {
    return this.countrieService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCountrieDto: UpdateCountrieDto) {
    return this.countrieService.update(id, updateCountrieDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.countrieService.remove(+id);
  }
}
