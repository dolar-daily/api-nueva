import { Module } from "@nestjs/common";
import { CountrieService } from "./countrie.service";
import { CountrieController } from "./countrie.controller";
import { DatabaseModule } from "src/database/database.module";
import { countrieProvider } from "./countrie.provider";

@Module({
  imports: [DatabaseModule],
  controllers: [CountrieController],
  providers: [...countrieProvider, CountrieService],
})
export class CountrieModule {}
