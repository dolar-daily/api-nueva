import { TYPES_MOVEMENTS } from 'src/enums/types-movements.enum';
import { Fund } from 'src/funds/entities/fund.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Transaction,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Movement {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'enum', enum: TYPES_MOVEMENTS })
  type: TYPES_MOVEMENTS;

  @Column({ type: 'decimal', precision: 10, scale: 2, default: 0.0 })
  old_amount: number;

  @Column({ type: 'decimal', precision: 10, scale: 2, default: 0.0 })
  new_amount: number;

  @Column({ type: 'decimal', precision: 10, scale: 2, default: 0.0 })
  movement_amount: number;

  @ManyToOne(() => Fund, (fund) => fund.movements)
  fund: Fund;

  @Column()
  description: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;
}
