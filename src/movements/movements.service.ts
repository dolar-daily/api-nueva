import { Inject, Injectable } from '@nestjs/common';
import { CreateMovementDto } from './dto/create-movement.dto';
import { UpdateMovementDto } from './dto/update-movement.dto';
import { Repository } from 'typeorm';
import { Movement } from './entities/movement.entity';

@Injectable()
export class MovementsService {
  constructor(
    @Inject('MOVEMENTS_REPOSITORY')
    private movementRepository: Repository<Movement>,
  ) {}

  create(createMovementDto: CreateMovementDto) {
    return 'This action adds a new movement';
  }

  findAll() {
    return `This action returns all movements`;
  }

  async findOne(id) {
    let data = await this.movementRepository
      .createQueryBuilder('movements')
      .where('movements.fundId = :id', { id })
      .getMany();

    return { data, message: 'Movimientos obtenidos con exito' };
  }

  update(id: number, updateMovementDto: UpdateMovementDto) {
    return `This action updates a #${id} movement`;
  }

  remove(id: number) {
    return `This action removes a #${id} movement`;
  }
}
