import { Connection } from "typeorm";
import { Logger } from "@nestjs/common";
import { Movement } from "./entities/movement.entity";

const logger = new Logger("MOVEMENTS PROVIDER");

export const movementProvider = [
  {
    provide: "MOVEMENTS_REPOSITORY",
    useFactory: (conecction: Connection) => {
      if (!conecction) {
        logger.error("La conexión a la base de datos no está definida");
        return null;
      }

      try {
        logger.log("Se ha cargado correctamente el Modelo: Bank")
        return conecction.getRepository(Movement);
      } catch (error) {
        logger.error("Error al cargar el Modelo: Movement", error);
      }
    },
    inject: ["MYSQL_CONNECT"],
  },
];
