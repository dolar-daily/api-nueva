import { Module } from '@nestjs/common';
import { MovementsService } from './movements.service';
import { MovementsController } from './movements.controller';
import { movementProvider } from './movements.provider';
import { DatabaseModule } from 'src/database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [MovementsController],
  providers: [...movementProvider, MovementsService],
})
export class MovementsModule {}
