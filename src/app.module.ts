import { Module, OnApplicationBootstrap } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { DatabaseModule } from "./database/database.module";
import { UserModule } from './user/user.module';
import { CompanyModule } from './company/company.module';
import { AuthModule } from './auth/auth.module';
import { usersProvider } from "./user/users.provider";
import { UserSeeder } from "./seed/user.seed";
import { companyProvider } from "./company/company.provider";
import { BankModule } from './bank/bank.module';
import { CountrieModule } from './countrie/countrie.module';
import { FundsModule } from './funds/funds.module';
import { TransactionsModule } from './transactions/transactions.module';
import { TransactionsTrackinsModule } from './transactions-trackins/transactions-trackins.module';
import { MovementsModule } from './movements/movements.module';
import { RechargesModule } from './recharges/recharges.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [
        '.prod.env',
        '.dev.env',
        '.env'
      ]
    }),
    DatabaseModule,
    UserModule,
    CompanyModule,
    AuthModule,
    BankModule,
    CountrieModule,
    FundsModule,
    TransactionsModule,
    TransactionsTrackinsModule,
    MovementsModule,
    RechargesModule,
  ],
  controllers: [],
  providers: [...usersProvider, ...companyProvider, UserSeeder],
})
export class AppModule implements OnApplicationBootstrap {
  constructor(private readonly userSeeder: UserSeeder){}

  onApplicationBootstrap() {
    this.userSeeder.seedUser()
  }
}
