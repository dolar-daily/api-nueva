import { IsAlpha, IsString } from "class-validator";

export class CreateBankDto {
  @IsString()
  code: string;

  @IsString({
    message: 'name: El nombre del banco no puede contener numeros'
  })
  name: string;
}
