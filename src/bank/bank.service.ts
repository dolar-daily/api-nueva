import { ConflictException, Inject, Injectable, NotFoundException } from "@nestjs/common";
import { CreateBankDto } from "./dto/create-bank.dto";
import { UpdateBankDto } from "./dto/update-bank.dto";
import { Bank } from "./entities/bank.entity";
import { Repository } from "typeorm";

@Injectable()
export class BankService {
  constructor(
    @Inject("BANK_REPOSITORY")
    private readonly bankRepository: Repository<Bank>
  ) {}

  async create(createBankDto: CreateBankDto) {
    const findBank = await this.bankRepository.findOne({
      where: { code: createBankDto.code },
    });
    if (findBank) {
      throw new ConflictException(
        "Este codigo de banco ya existe, intente con otro."
      );
    }

    const bank = await this.bankRepository.save(createBankDto);
    return { data: bank, message: "Banco creado con exito" };
  }

  async findAll() {
    const banks = await this.bankRepository.find()

    return { data: banks, message: "Bancos obtenidos con exito" };
  }

  async findOne(id: string) {
    const bankFind = await this.bankRepository.findOne({where: {id}})
    if(!bankFind){
      throw new NotFoundException("El banco que esta buscando no existe")
    }

    return {data: bankFind, message: "Banco encontrado con exito."}
  }

  update(id: number, updateBankDto: UpdateBankDto) {
    return `This action updates a #${id} bank`;
  }

  remove(id: string) {
    return `This action removes a #${id} bank`;
  }
}
