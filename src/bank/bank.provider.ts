import { Connection } from "typeorm";
import { Logger } from "@nestjs/common";
import { Bank } from "./entities/bank.entity";

const logger = new Logger("COMPANY PROVIDER");

export const bankProvider = [
  {
    provide: "BANK_REPOSITORY",
    useFactory: (conecction: Connection) => {
      if (!conecction) {
        logger.error("La conexión a la base de datos no está definida");
        return null;
      }

      try {
        logger.log("Se ha cargado correctamente el Modelo: Bank")
        return conecction.getRepository(Bank);
      } catch (error) {
        logger.error("Error al cargar el Modelo: Bank", error);
      }
    },
    inject: ["MYSQL_CONNECT"],
  },
];
