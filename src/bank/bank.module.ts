import { Module } from "@nestjs/common";
import { BankService } from "./bank.service";
import { BankController } from "./bank.controller";
import { DatabaseModule } from "src/database/database.module";
import { bankProvider } from "./bank.provider";

@Module({
  imports: [DatabaseModule],
  controllers: [BankController],
  providers: [...bankProvider, BankService],
})
export class BankModule {}
