import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UseGuards } from '@nestjs/common';
import { BankService } from './bank.service';
import { CreateBankDto } from './dto/create-bank.dto';
import { UpdateBankDto } from './dto/update-bank.dto';
import { ApiTags } from '@nestjs/swagger';
import { TransformInterceptor } from 'src/interceptors/response.interceptor';
import { UuidValidatorPipe } from 'src/pipes/validations/client-id-validation/uuid-validator.pipe';
import { AuthGuard } from 'src/auth/guard/auth.guard';

@Controller('banks')
@UseGuards(AuthGuard)
@UseInterceptors(TransformInterceptor)
@ApiTags('Bancos')
export class BankController {
  constructor(private readonly bankService: BankService) {}

  @Post()
  create(@Body() createBankDto: CreateBankDto) {
    return this.bankService.create(createBankDto);
  }

  @Get()
  findAll() {
    return this.bankService.findAll();
  }

  @Get(":id")
  findOne(@Param("id", new UuidValidatorPipe()) id: string) {
    return this.bankService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBankDto: UpdateBankDto) {
    return this.bankService.update(+id, updateBankDto);
  }

  @Delete(':id')
  remove(@Param("id", new UuidValidatorPipe()) id: string) {
    return this.bankService.remove(id);
  }
}
