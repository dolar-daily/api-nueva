import { Company } from "src/company/entities/company.entity";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    full_name: string

    @Column()
    user: string

    @Column()
    password: string

    @Column({default: 'integrator'})
    role: string

    @ManyToOne(() => Company, (company) => company.users, {nullable: true})
    company: Company

    @OneToMany(() => Transaction, (transaction) => transaction.company)
    transactions: Transaction[]

    @CreateDateColumn()
    created_at: Date

    @UpdateDateColumn()
    updated_at: Date

    @DeleteDateColumn()
    deleted_at: Date
}
