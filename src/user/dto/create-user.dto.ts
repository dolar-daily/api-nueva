import { IsAlpha, IsOptional, IsString } from 'class-validator';
import { Company } from 'src/company/entities/company.entity';

export class CreateUserDto {
  @IsAlpha('es-ES', {
    message: 'El nombre no puede contener numeros',
  })
  full_name;

  @IsString({
    message: 'El usuario debe ser un string, y es obligatorio.',
  })
  user;

  @IsString({
    message: 'La clave debe ser un string, y es obligatorio.',
  })
  password;

  @IsOptional()
  company: Company;

  @IsOptional()
  role: string;
}
