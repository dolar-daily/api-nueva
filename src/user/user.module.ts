import { Module } from "@nestjs/common";
import { UserService } from "./user.service";
import { UserController } from "./user.controller";
import { usersProvider } from "./users.provider";
import { DatabaseModule } from "src/database/database.module";

@Module({
  imports: [DatabaseModule],
  controllers: [UserController],
  providers: [...usersProvider, UserService],
})
export class UserModule { }
