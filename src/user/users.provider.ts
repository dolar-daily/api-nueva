import { Connection } from "typeorm";
import { Logger } from "@nestjs/common";
import { User } from "./entities/user.entity";

const logger = new Logger("USER PROVIDER");

export const usersProvider = [
  {
    provide: "USER_REPOSITORY",
    useFactory: (conecction: Connection) => {
      if (!conecction) {
        logger.error("La conexión a la base de datos no está definida");
        return null;
      }

      try {
        logger.log("Se ha cargado correctamente el Modelo: User")
        return conecction.getRepository(User);
      } catch (error) {
        logger.error("Error al cargar el Modelo: User", error);
      }
    },
    inject: ["MYSQL_CONNECT"],
  },
];
