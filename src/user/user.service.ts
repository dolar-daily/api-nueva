import { Inject, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import * as bcryptjs from 'bcryptjs';

@Injectable()
export class UserService {
  constructor(
    @Inject('USER_REPOSITORY')
    private readonly userRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const hashedPassword = await bcryptjs.hash(createUserDto.password, 10);
    const dataToSave = { ...createUserDto, password: hashedPassword };

    const data = await this.userRepository.save(dataToSave);

    return { data, message: 'Usuario Creado con éxito' };
  }

  async findAll() {
    let data = await this.userRepository.find();
    return { data, message: 'Listado de usuarios obtenidos con exito' };
  }

  findOne(id: number) {
    return `This action returns a #${id} user`;
  }

  async update(id, updateUserDto: UpdateUserDto) {
    let pass = '';
    if (updateUserDto.password) {
      pass = await bcryptjs.hash(updateUserDto.password, 10);
      await this.userRepository.update(id, { password: pass });
    }

    let data = await this.userRepository.update(id, {
      full_name: updateUserDto.full_name,
      company: updateUserDto.company,
      user: updateUserDto.user,
    });

    return {
      data,
      message: `Usuario #${id} actualizado con éxito`,
    };
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
