import { Fund } from "src/funds/entities/fund.entity";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { User } from "src/user/entities/user.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Company {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    name: string

    @Column()
    representative: string

    @Column()
    address: string

    @Column()
    phone: string

    @OneToMany(() => User, (user) => user.company)
    users: User[]

    @OneToMany(() => Fund, (fund) => fund.company)
    funds: Fund[]

    @OneToMany(() => Transaction, (transaction) => transaction.company)
    transactions: Transaction[]

    @CreateDateColumn()
    created_at: Date

    @UpdateDateColumn()
    updated_at: Date

    @DeleteDateColumn()
    deleted_at: Date
}
