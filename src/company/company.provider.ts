import { Connection } from "typeorm";
import { Logger } from "@nestjs/common";
import { Company } from "./entities/company.entity";

const logger = new Logger("COMPANY PROVIDER");

export const companyProvider = [
  {
    provide: "COMPANY_REPOSITORY",
    useFactory: (conecction: Connection) => {
      if (!conecction) {
        logger.error("La conexión a la base de datos no está definida");
        return null;
      }

      try {
        logger.log("Se ha cargado correctamente el Modelo: Company")
        return conecction.getRepository(Company);
      } catch (error) {
        logger.error("Error al cargar el Modelo: Company", error);
      }
    },
    inject: ["MYSQL_CONNECT"],
  },
];
