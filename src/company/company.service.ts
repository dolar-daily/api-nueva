import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { CreateCompanyDto } from './dto/create-company.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';
import { Repository } from 'typeorm';
import { Company } from './entities/company.entity';
import { Transaction } from 'src/transactions/entities/transaction.entity';

@Injectable()
export class CompanyService {
  constructor(
    @Inject('COMPANY_REPOSITORY')
    private readonly companyRepository: Repository<Company>,
    @Inject('TRANSACTION_REPOSITORY')
    private transactionRepository: Repository<Transaction>,
  ) {}

  async create(createCompanyDto: CreateCompanyDto) {
    const data = await this.companyRepository.save(createCompanyDto);

    return { data, message: 'Compañia creada con exito' };
  }

  async findAll() {
    const data = await this.companyRepository.find({ relations: ['funds'] });

    return { data, message: 'Compañias obtenidas con exito' };
  }

  async findOne(id: string) {
    const data = await this.companyRepository.findOne({ where: { id } });
    if (!data) {
      throw new NotFoundException('No se ha encontrado la empresa');
    }

    return { data, message: 'Empresa obtenida con exito' };
  }

  update(id: string, updateCompanyDto: UpdateCompanyDto) {
    const data = this.companyRepository.update(id, updateCompanyDto);

    return { data, message: 'Empresa actualizada con exito' };
  }

  remove(id: string) {
    const data = this.companyRepository.softDelete(id);

    return { data, message: 'Empresa desactivada con exito' };
  }

  async info(id, company) {
    const [totalTransactions, createdTransactions, completedTransactions] =
      await Promise.all([
        this.transactionRepository
          .createQueryBuilder('transaction')
          .where('transaction.companyId = :id', { id })
          .getCount(),
        this.transactionRepository
          .createQueryBuilder('transaction')
          .where('transaction.companyId = :id', { id })
          .andWhere('transaction.status = :status', { status: 'CREADA' })
          .getCount(),
        this.transactionRepository
          .createQueryBuilder('transaction')
          .where('transaction.companyId = :id', { id })
          .andWhere('transaction.status = :status', { status: 'COMPLETADA' })
          .getCount(),
      ]);

    const fund = await this.companyRepository.findOne({
      where: { id },
      relations: ['funds'],
    });

    return {
      data: {
        company: fund,
        totalTransactions,
        createdTransactions,
        completedTransactions,
      },
      message: 'Informacion obtenida con exito',
    };
  }
}
