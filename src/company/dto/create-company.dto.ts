import { IsNotEmpty, IsString } from "class-validator";

export class CreateCompanyDto {
  @IsString()
  @IsNotEmpty({
    message: 'name: Nombre no puede quedar en blanco'
  })
  name: string;
  
  @IsString()
  @IsNotEmpty({
    message: 'representative: Representante no puede quedar en blanco'
  })
  representative: string;

  @IsString()
  @IsNotEmpty({
    message: 'address: Direccion no puede quedar en blanco'
  })
  address: string;

  @IsString()
  @IsNotEmpty({
    message: 'phone: Telefono no puede ser en blanco'
  })
  phone: string;
}
