import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UseGuards,
  Request,
} from '@nestjs/common';
import { CompanyService } from './company.service';
import { CreateCompanyDto } from './dto/create-company.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';
import { ApiExcludeController, ApiTags } from '@nestjs/swagger';
import { TransformInterceptor } from 'src/interceptors/response.interceptor';
import { AuthGuard } from 'src/auth/guard/auth.guard';
import { UuidValidatorPipe } from 'src/pipes/validations/client-id-validation/uuid-validator.pipe';

@ApiExcludeController()
@ApiTags('Compañias')
@UseGuards(AuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller('company')
export class CompanyController {
  constructor(private readonly companyService: CompanyService) {}

  @Post()
  create(@Body() createCompanyDto: CreateCompanyDto) {
    return this.companyService.create(createCompanyDto);
  }

  @Get()
  findAll() {
    return this.companyService.findAll();
  }

  @Get('info')
  info(@Request() req) {
    return this.companyService.info(req.user.company.id, req.user.company);
  }

  @Get(':id')
  findOne(@Param('id', new UuidValidatorPipe()) id: string) {
    return this.companyService.findOne(id);
  }

  @Patch(':id')
  update(
    @Param('id', new UuidValidatorPipe()) id: string,
    @Body() updateCompanyDto: UpdateCompanyDto,
  ) {
    return this.companyService.update(id, updateCompanyDto);
  }

  @Delete(':id')
  remove(@Param('id', new UuidValidatorPipe()) id: string) {
    return this.companyService.remove(id);
  }
}
