import { Module } from "@nestjs/common";
import { CompanyService } from "./company.service";
import { CompanyController } from "./company.controller";
import { companyProvider } from "./company.provider";
import { DatabaseModule } from "src/database/database.module";
import { transactionProvider } from "src/transactions/transactions.provider";

@Module({
  imports: [DatabaseModule],
  controllers: [CompanyController],
  providers: [...companyProvider, ...transactionProvider, CompanyService],
})
export class CompanyModule {}
