import {
  ArgumentMetadata,
  Injectable,
  NotAcceptableException,
  ParseUUIDPipe,
  PipeTransform,
} from "@nestjs/common";

@Injectable()
export class UuidValidatorPipe implements PipeTransform {
  constructor(private errorMessage = "El uuid no es valido.") {}

  async transform(clienId: string, metadata: ArgumentMetadata) {
    const uuidPipe = new ParseUUIDPipe();

    try {
      return await uuidPipe.transform(clienId, metadata);
    } catch (error) {
      throw new NotAcceptableException(this.errorMessage);
    }
  }
}
