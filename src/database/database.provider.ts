import { Logger } from "@nestjs/common";
import { Bank } from "src/bank/entities/bank.entity";
import { Company } from "src/company/entities/company.entity";
import { Countrie } from "src/countrie/entities/countrie.entity";
import { Fund } from "src/funds/entities/fund.entity";
import { Movement } from "src/movements/entities/movement.entity";
import { TransactionsTrackin } from "src/transactions-trackins/entities/transactions-trackin.entity";
import { Transaction } from "src/transactions/entities/transaction.entity";
import { User } from "src/user/entities/user.entity";
import { createConnection } from "typeorm";

const logger = new Logger("DATABASE PROVIDER");

export const databaseProviders = [
  {
    provide: "MYSQL_CONNECT",
    useFactory: async () => {
      try {
        const connection = await createConnection({
          type: "mysql",
          host: process.env.MYSQL_HOST,
          port: Number(process.env.MYSQL_PORT),
          username: process.env.MYSQL_USERNAME,
          password: process.env.MYSQL_PASSWORD,
          database: process.env.MYSQL_DATABASE,
          entities: [User, Company, Bank, Countrie, Fund, Transaction, TransactionsTrackin, Movement],
          synchronize: true,
        });
        logger.debug(
          `Conexion con database ${process.env.MYSQL_DATABASE} ha sido exitosa`
        );
        return connection;
      } catch (error) {
        logger.debug("Error al conectar: ", error);
        throw error;
      }
    },
  },
];