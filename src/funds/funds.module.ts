import { Module } from '@nestjs/common';
import { FundsService } from './funds.service';
import { FundsController } from './funds.controller';
import { DatabaseModule } from 'src/database/database.module';
import { fundProvider } from './funds.provider';
import { movementProvider } from 'src/movements/movements.provider';

@Module({
  imports: [DatabaseModule],
  controllers: [FundsController],
  providers: [...fundProvider, ...movementProvider, FundsService],
})
export class FundsModule {}
