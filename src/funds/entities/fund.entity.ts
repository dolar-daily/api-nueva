import { Company } from "src/company/entities/company.entity";
import { Countrie } from "src/countrie/entities/countrie.entity";
import { Movement } from "src/movements/entities/movement.entity";
import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  ManyToOne,
  Column,
} from "typeorm";

@Entity()
export class Fund {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({ type: 'decimal', precision: 15, scale: 4 })
  amount: number;

  @ManyToOne(() => Company, (company) => company.funds)
  company: Company;

  @ManyToOne(() => Countrie, (countrie) => countrie.funds)
  countrie: Countrie;

  @OneToMany(() => Movement, (movement) => movement.fund)
  movements: Movement[]

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;
}
