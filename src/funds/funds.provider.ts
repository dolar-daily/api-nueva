import { Connection } from "typeorm";
import { Logger } from "@nestjs/common";
import { Fund } from "./entities/fund.entity";

const logger = new Logger("FUNDS PROVIDER");

export const fundProvider = [
  {
    provide: "FUND_REPOSITORY",
    useFactory: (conecction: Connection) => {
      if (!conecction) {
        logger.error("La conexión a la base de datos no está definida");
        return null;
      }

      try {
        logger.log("Se ha cargado correctamente el Modelo: Funds")
        return conecction.getRepository(Fund);
      } catch (error) {
        logger.error("Error al cargar el Modelo: Funds", error);
      }
    },
    inject: ["MYSQL_CONNECT"],
  },
];
