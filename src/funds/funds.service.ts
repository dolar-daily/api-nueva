import {
  ConflictException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateFundDto } from './dto/create-fund.dto';
import { UpdateFundDto } from './dto/update-fund.dto';
import { Repository } from 'typeorm';
import { Fund } from './entities/fund.entity';
import { Movement } from 'src/movements/entities/movement.entity';
import { TYPES_MOVEMENTS } from 'src/enums/types-movements.enum';

@Injectable()
export class FundsService {
  constructor(
    @Inject('FUND_REPOSITORY')
    private fundsRepository: Repository<Fund>,
    @Inject('MOVEMENTS_REPOSITORY')
    private movementRepository: Repository<Movement>,
  ) {}
  async create(createFundDto: CreateFundDto) {
    const findFund = await this.fundsRepository
      .createQueryBuilder('fund')
      .where('fund.companyId = :companyId AND fund.countrieId = :countrieId', {
        countrieId: createFundDto.countrie,
        companyId: createFundDto.company,
      })
      .getOne();

    if (findFund) {
      throw new ConflictException(
        'Esta compañia ya tiene un wallet para este pais, proceda a agregarle saldo.',
      );
    }
    const data = await this.fundsRepository.save(createFundDto);

    return { data, message: 'Fondo creado con exito' };
  }

  async findAll() {
    const data = await this.fundsRepository.find({
      relations: ['company', 'countrie'],
    });
    return { data, message: 'Fondos obtenidos con exito' };
  }

  async findOne(user) {
    const data = await this.fundsRepository
      .createQueryBuilder('fund')
      .innerJoinAndSelect('fund.countrie', 'countrie')
      .where('fund.companyId = :companyId', {
        companyId: user.company
          ? user.company.id
          : 'd584b518-ad7a-47a6-a10b-a0cb7db52c5b',
      })
      .getOne();

    if (!data) {
      throw new NotFoundException(
        'No Cuentas con ningun fondo asignado contacta con soporte!',
      );
    }

    return { data, message: 'Fondos obtenidos con éxito' };
  }

  async update(id, updateFundDto: UpdateFundDto) {
    let data = await this.fundsRepository.update(id, {
      amount: updateFundDto.amount,
    });

    await this.movementRepository.save({
      type: TYPES_MOVEMENTS.AJUSTE,
      old_amount: updateFundDto.old_amount,
      new_amount: updateFundDto.amount,
      movement_amount: 0,
      fund: id,
      description: updateFundDto.description
    });

    return { data, message: 'Actualizado con exito' };
  }

  remove(id: number) {
    return `This action removes a #${id} fund`;
  }
}
