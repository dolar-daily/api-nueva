import { PartialType } from '@nestjs/swagger';
import { CreateFundDto } from './create-fund.dto';
import { IsOptional } from 'class-validator';

export class UpdateFundDto extends PartialType(CreateFundDto) {
  @IsOptional()
  description: string;

  @IsOptional()
  old_amount: number;
}
