import { IsNumber, IsOptional, IsString } from 'class-validator';
import { Company } from 'src/company/entities/company.entity';
import { Countrie } from 'src/countrie/entities/countrie.entity';

export class CreateFundDto {
  @IsNumber()
  amount: number;

  @IsString()
  company: Company;

  @IsString()
  countrie: Countrie;

}
