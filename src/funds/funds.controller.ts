import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UseGuards, Request } from '@nestjs/common';
import { FundsService } from './funds.service';
import { CreateFundDto } from './dto/create-fund.dto';
import { UpdateFundDto } from './dto/update-fund.dto';
import { TransformInterceptor } from 'src/interceptors/response.interceptor';
import { AuthGuard } from 'src/auth/guard/auth.guard';

@Controller('funds')
@UseGuards(AuthGuard)
@UseInterceptors(TransformInterceptor)
export class FundsController {
  constructor(private readonly fundsService: FundsService) {}

  @Post()
  create(@Body() createFundDto: CreateFundDto) {
    return this.fundsService.create(createFundDto);
  }

  @Get()
  findAll() {
    return this.fundsService.findAll();
  }

  @Get('/company')
  findOne(@Request() req) {
    return this.fundsService.findOne(req.user);
  }

  @Patch(':id')
  update(@Param('id') id, @Body() updateFundDto: UpdateFundDto) {
    return this.fundsService.update(id, updateFundDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.fundsService.remove(+id);
  }
}
