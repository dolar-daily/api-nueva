import { PartialType } from '@nestjs/swagger';
import { CreateTransactionsTrackinDto } from './create-transactions-trackin.dto';

export class UpdateTransactionsTrackinDto extends PartialType(CreateTransactionsTrackinDto) {}
