import { Module } from "@nestjs/common";
import { TransactionsTrackinsService } from "./transactions-trackins.service";
import { TransactionsTrackinsController } from "./transactions-trackins.controller";
import { DatabaseModule } from "src/database/database.module";
import { transactionTrackingsProvider } from "./transactions-trackins.provider";

@Module({
  imports: [DatabaseModule],
  controllers: [TransactionsTrackinsController],
  providers: [...transactionTrackingsProvider, TransactionsTrackinsService],
})
export class TransactionsTrackinsModule {}
