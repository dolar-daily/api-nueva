import { Injectable } from '@nestjs/common';
import { CreateTransactionsTrackinDto } from './dto/create-transactions-trackin.dto';
import { UpdateTransactionsTrackinDto } from './dto/update-transactions-trackin.dto';

@Injectable()
export class TransactionsTrackinsService {
  create(createTransactionsTrackinDto: CreateTransactionsTrackinDto) {
    return 'This action adds a new transactionsTrackin';
  }

  findAll() {
    return `This action returns all transactionsTrackins`;
  }

  findOne(id: number) {
    return `This action returns a #${id} transactionsTrackin`;
  }

  update(id: number, updateTransactionsTrackinDto: UpdateTransactionsTrackinDto) {
    return `This action updates a #${id} transactionsTrackin`;
  }

  remove(id: number) {
    return `This action removes a #${id} transactionsTrackin`;
  }
}
