import { STATUS_TRANSACTION } from "src/enums/status-transactions.enum";
import { Transaction } from "src/transactions/entities/transaction.entity";
import {
    Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class TransactionsTrackin {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({type: 'enum', enum: STATUS_TRANSACTION})
  status: STATUS_TRANSACTION
  
  @ManyToOne(() => Transaction, (transaction) => transaction.trackings)
  transaction: Transaction

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;
}
