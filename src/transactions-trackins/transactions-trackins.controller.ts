import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, UseInterceptors } from '@nestjs/common';
import { TransactionsTrackinsService } from './transactions-trackins.service';
import { CreateTransactionsTrackinDto } from './dto/create-transactions-trackin.dto';
import { UpdateTransactionsTrackinDto } from './dto/update-transactions-trackin.dto';
import { AuthGuard } from 'src/auth/guard/auth.guard';
import { TransformInterceptor } from 'src/interceptors/response.interceptor';

@Controller('transactions-trackins')
@UseInterceptors(TransformInterceptor)
@UseGuards(AuthGuard)
export class TransactionsTrackinsController {
  constructor(private readonly transactionsTrackinsService: TransactionsTrackinsService) {}

  @Post()
  create(@Body() createTransactionsTrackinDto: CreateTransactionsTrackinDto) {
    return this.transactionsTrackinsService.create(createTransactionsTrackinDto);
  }

  @Get()
  findAll() {
    return this.transactionsTrackinsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.transactionsTrackinsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTransactionsTrackinDto: UpdateTransactionsTrackinDto) {
    return this.transactionsTrackinsService.update(+id, updateTransactionsTrackinDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.transactionsTrackinsService.remove(+id);
  }
}
