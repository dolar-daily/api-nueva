import { Connection } from "typeorm";
import { Logger } from "@nestjs/common";
import { TransactionsTrackin } from "./entities/transactions-trackin.entity";

const logger = new Logger("TRANSACTION TRACKINGS PROVIDER");

export const transactionTrackingsProvider = [
  {
    provide: "TRANSACTION_TRACKINS_REPOSITORY",
    useFactory: (conecction: Connection) => {
      if (!conecction) {
        logger.error("La conexión a la base de datos no está definida");
        return null;
      }

      try {
        logger.log("Se ha cargado correctamente el Modelo: TransactionsTrackin")
        return conecction.getRepository(TransactionsTrackin);
      } catch (error) {
        logger.error("Error al cargar el Modelo: TransactionsTrackin", error);
      }
    },
    inject: ["MYSQL_CONNECT"],
  },
];
