import { Inject, Injectable } from "@nestjs/common";
import * as bcryptjs from "bcryptjs";
import { Company } from "src/company/entities/company.entity";
import { User } from "src/user/entities/user.entity";
import { Repository } from "typeorm";

@Injectable()
export class UserSeeder {
    constructor(
        @Inject("USER_REPOSITORY")
        private readonly userRepository: Repository<User>,
        @Inject("COMPANY_REPOSITORY")
        private readonly companyRepository: Repository<Company>
    ) { }

    async seedUser() {
        let findUser = await this.userRepository.findOne({ where: { user: 'sistem' } })
        if (!findUser) {
            let plainPassword = '1256admin'
            const hashPassword = await bcryptjs.hash(plainPassword, 10)
            this.userRepository.save({
                full_name: 'System',
                user: 'sistem',
                password: hashPassword,
                role: 'admin'
            })
            plainPassword = 'integrator'
            const integratorPassword = await bcryptjs.hash(plainPassword, 10)
            const company = await this.companyRepository.save({
                name: 'DEMO',
                representative: 'DEMO REPRESENT',
                address: 'DEMO ADDRESS',
                phone: '99999999999'
            })

            this.userRepository.save({
                full_name: 'Integrator DEMO',
                user: 'integrator',
                password: integratorPassword,
                role: 'integrator',
                company: company
            })
        }

        findUser = await this.userRepository.findOne({ where: { user: 'rebelec3459HJ34' } })
        if (!findUser) {
            let plainPassword = 'ZY2$S]FJtyXzf[cV)PLV(ci{F0R9k*'
            const integratorPassword = await bcryptjs.hash(plainPassword, 10)
            const company = await this.companyRepository.save({
                name: 'REBELEC',
                representative: 'MARCELO',
                address: 'ECUADOR',
                phone: '00000000'
            })

            this.userRepository.save({
                full_name: 'REBELEC',
                user: 'rebelec3459HJ34',
                password: integratorPassword,
                role: 'integrator',
                company: company
            })
        }
    }
}