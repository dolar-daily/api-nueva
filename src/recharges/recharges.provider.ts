import { Connection } from 'typeorm';
import { Logger } from '@nestjs/common';
import { Recharge } from './entities/recharge.entity';

const logger = new Logger('RECHARGE PROVIDER');

export const rechargeProvider = [
  {
    provide: 'RECHARGES_REPOSITORY',
    useFactory: (conecction: Connection) => {
      if (!conecction) {
        logger.error('La conexión a la base de datos no está definida');
        return null;
      }

      try {
        logger.log('Se ha cargado correctamente el Modelo: Recharge');
        return conecction.getRepository(Recharge);
      } catch (error) {
        logger.error('Error al cargar el Modelo: Recharge', error);
      }
    },
    inject: ['MYSQL_CONNECT'],
  },
];
