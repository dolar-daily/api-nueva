import { Module } from '@nestjs/common';
import { RechargesService } from './recharges.service';
import { RechargesController } from './recharges.controller';
import { DatabaseModule } from 'src/database/database.module';
import { rechargeProvider } from './recharges.provider';

@Module({
  imports: [DatabaseModule],
  controllers: [RechargesController],
  providers: [...rechargeProvider, RechargesService],
})
export class RechargesModule {}
