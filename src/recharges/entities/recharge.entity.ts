import { Column, CreateDateColumn, DeleteDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class Recharge {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'decimal', precision: 15, scale: 2 })
  amount_recharge: number;

  @Column({ type: 'decimal', precision: 15, scale: 3 })
  utility_panet: number;

  @Column({ type: 'decimal', precision: 15, scale: 3 })
  sale_usdt: number;

  @Column({ type: 'decimal', precision: 15, scale: 3 })
  sale_rate: number;

  @Column({ type: 'decimal', precision: 15, scale: 3 })
  gross_rate: number;

  @Column({ type: 'decimal', precision: 15, scale: 3 })
  public_rate: number;

  @Column({ type: 'decimal', precision: 15, scale: 3 })
  amount_bs: number;

  @Column({ type: 'decimal', precision: 15, scale: 3 })
  raise_total: number;

  @Column({ type: 'decimal', precision: 15, scale: 3 })
  utility_client: number;

  @Column({ type: 'decimal', precision: 15, scale: 3 })
  amount_usd: number;
  
  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;
}
