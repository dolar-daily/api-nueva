import { Module } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { AuthController } from "./auth.controller";
import { DatabaseModule } from "src/database/database.module";
import { usersProvider } from "src/user/users.provider";
import { JwtModule } from "@nestjs/jwt";
import { jwtConstants } from "./constants";

@Module({
  imports: [
    DatabaseModule,
    JwtModule.register({
      global: true,
      secret: 's$c4t567',
      signOptions: { expiresIn: "1d" },
    }),
  ],
  controllers: [AuthController],
  providers: [...usersProvider, AuthService],
})
export class AuthModule {}
