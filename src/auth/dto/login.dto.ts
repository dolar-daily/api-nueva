import { IsString } from "class-validator";

export class LoginDto {
  @IsString({
    message: "user: El usuario debe ser un string, y es obligatorio.",
  })
  user;

  @IsString({
    message: "password: La clave debe ser un string, y es obligatorio.",
  })
  password;
}
