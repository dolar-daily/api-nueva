import { Body, Controller, Get, Post, UseGuards, Request, UseInterceptors } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { LoginDto } from "./dto/login.dto";
import { AuthGuard } from "./guard/auth.guard";
import { TransformInterceptor } from "src/interceptors/response.interceptor";
import { ApiTags } from "@nestjs/swagger";

@Controller("auth")
@ApiTags("Autenticacion")
@UseInterceptors(TransformInterceptor)
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post("login")
  login(@Body() loginDto: LoginDto) {
    return this.authService.login(loginDto);
  }

  @Get("profile")
  @UseGuards(AuthGuard)
  profile(@Request() req) {
    return {data: req.user, message: 'Perfil obtenido con exito'};
  }
}
