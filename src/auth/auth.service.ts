import {
  Inject,
  Injectable,
  Logger,
  NotFoundException,
  UnauthorizedException,
} from "@nestjs/common";
import { User } from "src/user/entities/user.entity";
import { Repository } from "typeorm";
import { LoginDto } from "./dto/login.dto";
import * as bcryptjs from "bcryptjs";
import { JwtService } from "@nestjs/jwt";

@Injectable()
export class AuthService {
  private logger = new Logger("SERVICIO AUTENTICACION");
  constructor(
    @Inject("USER_REPOSITORY")
    private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService
  ) {}

  async login({ user, password }: LoginDto) {
    const findUser = await this.userRepository.findOne({
      where: { user },
      relations: ["company"],
    });
    if (!findUser) {
      throw new NotFoundException("Usuario no existe");
    }

    const isPasswordValid = await bcryptjs.compare(password, findUser.password);

    if (!isPasswordValid) {
      throw new UnauthorizedException("Clave incorrecta");
    }

    let playload = {
      id: findUser.id,
      full_name: findUser.full_name,
      user: findUser.user,
      role: findUser.role,
      token: null,
      company: findUser.company
    };

    playload.token = await this.jwtService.signAsync(playload);

    return { data: playload, message: "Sesion iniciada con exito" };
  }
}
